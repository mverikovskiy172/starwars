using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using SmtpSender.Services;
using StarWars.Data;
using StarWars.Services;
using StarWars.Services.Configurations;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection") ??
                       throw new InvalidOperationException("Connection string 'DefaultConnection' not found.");

builder.Services.AddDbContext<ApplicationDbContext>(x =>
    x.UseSqlite(connectionString));

builder.Services.AddIdentity<IdentityUser, IdentityRole>(option =>
    {
        option.SignIn.RequireConfirmedAccount = false;
        option.Password.RequireDigit = false;
        option.Password.RequireLowercase = false;
        option.Password.RequireNonAlphanumeric = false;
        option.Password.RequireUppercase = false;
        option.Password.RequiredLength = 0;
    })
    .AddSignInManager<SignInManager<IdentityUser>>()
    .AddUserManager<UserManager<IdentityUser>>()
    .AddRoleManager<RoleManager<IdentityRole>>()
    .AddEntityFrameworkStores<ApplicationDbContext>();

builder.Services.AddControllersWithViews().AddRazorRuntimeCompilation();

builder.Services.AddAuthorization();

builder.Services.AddAutoMapper(typeof(StarWars.Services.Configurations.MapperConfiguration));

builder.Services.AddScoped<ICatalogService, CatalogService>();
builder.Services.AddScoped<IHeroService, HeroService>();
builder.Services.AddScoped<IAccountService, AccountService>();

var app = builder.Build();

await app.ConfigureIdentityAsync();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
	name: "default",
	pattern: "{controller=Catalog}/{action=Catalog}/{id?}");

app.Run();