﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using StarWars.Data;
using StarWars.Data.Models;
using StarWars.ViewModels;
using System.Security.Authentication;

namespace StarWars.Services
{
	public interface IHeroService
	{
		Task<HeroViewModel> GetHero(int heroId);
		Task<int> CreateHero(CreateHeroViewModel model, string userId);
		Task<CreateHeroViewModel> GetPageForCreateHero();
		Task<int> UpdateHero(int heroId, UpdateHeroViewModel model, string userId);
		Task<UpdateHeroViewModel> GetPageForUpdateHero(int heroId, string userId);
		Task<List<PlanetViewModel>> GetPlanets();
		Task<List<FilmViewModel>> GetFilms();
		Task DeleteHero(int heroId, string userId);
		Task<List<FilmViewModel>> GetFilmsHero(int heroId);
		Task<PlanetViewModel> GetPlanetHero(int heroId);
	}

	public class HeroService : IHeroService
	{
		private readonly ApplicationDbContext _dbContext;
		private readonly IMapper _mapper;

		public HeroService(ApplicationDbContext dbContext, IMapper mapper)
		{
			_dbContext = dbContext;
			_mapper = mapper;
		}

		public async Task<HeroViewModel> GetHero(int heroId)
		{
			var hero = await _dbContext.Heroes.Include(x => x.Films).Include(x => x.CreateByUser)
				.Include(x => x.Planet)
				.FirstOrDefaultAsync(x => x.Id == heroId);
			if (hero == null)
			{
				throw new KeyNotFoundException();
			}

			return _mapper.Map<HeroViewModel>(hero);
		}

		public async Task<int> CreateHero(CreateHeroViewModel model, string userId)
		{
			var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
			if (user == null)
			{
				throw new Exception();
			}

			var filmsHero = await _dbContext.Films.Where(x => model.SelectedFilms.Any(m => m == x.Id)).ToListAsync();
			var planetHero = await _dbContext.Planets.FirstOrDefaultAsync(x => x.Id == model.SelectedPlanet);
			if (planetHero == null)
			{
				throw new KeyNotFoundException();
			}

			var hero = _mapper.Map<Hero>(model);
			hero.Films = filmsHero;
			hero.Planet = planetHero;
			hero.CreateByUser = user;

			await _dbContext.Heroes.AddAsync(hero);
			await _dbContext.SaveChangesAsync();

			return hero.Id;
		}

		public async Task<List<PlanetViewModel>> GetPlanets()
		{
			return await _dbContext.Planets.Select(x => _mapper.Map<PlanetViewModel>(x)).ToListAsync();
		}

		public async Task<List<FilmViewModel>> GetFilms()
		{
			return await _dbContext.Films.Select(x => _mapper.Map<FilmViewModel>(x)).ToListAsync();
		}

		public async Task DeleteHero(int heroId, string userId)
		{
			var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
			if (user == null)
			{
				throw new Exception();
			}

			var hero = await _dbContext.Heroes.Include(x => x.CreateByUser).FirstOrDefaultAsync(x => x.Id == heroId);
			if (hero == null)
			{
				throw new KeyNotFoundException();
			}

			if (user.Id != hero.CreateByUser.Id)
			{
				throw new AuthenticationException();
			}

			_dbContext.Heroes.Remove(hero);
			await _dbContext.SaveChangesAsync();
		}

		public async Task<CreateHeroViewModel> GetPageForCreateHero()
		{
			var films = await GetFilms();

			var planets = await GetPlanets();

			var createHeroViewModel = new CreateHeroViewModel()
			{
				Films = films,
				Planets = planets,
			};

			return createHeroViewModel;
		}

		public async Task<int> UpdateHero(int heroId, UpdateHeroViewModel model, string userId)
		{
			var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
			if (user == null)
			{
				throw new Exception();
			}

			var hero = await _dbContext.Heroes.Include(x => x.CreateByUser).Include(x => x.Films).Include(x => x.Planet)
				.FirstOrDefaultAsync(x => x.Id == heroId);
			if (hero == null)
			{
				throw new KeyNotFoundException();
			}

			if (user.Id != hero.CreateByUser.Id)
			{
				throw new AuthenticationException();
			}

			var filmsHero = await _dbContext.Films.Where(x => model.SelectedFilms.Any(m => m == x.Id)).ToListAsync();
			var planetHero = await _dbContext.Planets.FirstOrDefaultAsync(x => x.Id == model.SelectedPlanet);

			if (planetHero == null)
			{
				throw new Exception();
			}

			var films = hero.Films.ToList();

			hero.Films.RemoveRange(0, films.Count);
			await _dbContext.SaveChangesAsync();

			hero.Name = model.Name;
			hero.NameOriginal = model.NameOriginal;
			hero.BornDate = model.BornDate;
			hero.Gender = model.Gender;
			hero.Race = model.Race;
			hero.Height = model.Height;
			hero.EyeColor = model.EyeColor;
			hero.HairColor = model.HairColor;
			hero.Films = filmsHero;
			hero.Planet = planetHero;
			hero.CreateByUser = user;

			await _dbContext.SaveChangesAsync();

			return hero.Id;
		}

		public async Task<UpdateHeroViewModel> GetPageForUpdateHero(int heroId, string userId)
		{
			var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
			if (user == null)
			{
				throw new Exception();
			}

			var hero = await GetHero(heroId);
			if (user.Id != hero.CreateByUser.Id)
			{
				throw new AuthenticationException();
			}

			var updateHeroViewModel = _mapper.Map<UpdateHeroViewModel>(hero);
			updateHeroViewModel.Planets = await GetPlanets();

			return updateHeroViewModel;
		}

		public async Task<List<FilmViewModel>> GetFilmsHero(int heroId)
		{
			var hero = await _dbContext.Heroes.Include(x => x.Films).FirstOrDefaultAsync(x => x.Id == heroId);
			if (hero == null)
			{
				throw new KeyNotFoundException();
			}

			return hero.Films.Select(x => _mapper.Map<FilmViewModel>(x)).ToList();
		}

		public async Task<PlanetViewModel> GetPlanetHero(int heroId)
		{
			var hero = await _dbContext.Heroes.Include(x => x.Planet).FirstOrDefaultAsync(x => x.Id == heroId);
			if (hero == null)
			{
				throw new KeyNotFoundException();
			}

			return _mapper.Map<PlanetViewModel>(hero.Planet);
		}
	}
}