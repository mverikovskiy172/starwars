﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using StarWars.Data;
using StarWars.ViewModels;
using X.PagedList;

namespace StarWars.Services
{
	public interface ICatalogService
	{
		Task<CatalogViewModel> GetCatalog(CatalogViewModel model, int page,
			int pageSize);
	}

	public class CatalogService : ICatalogService
	{
		private readonly ApplicationDbContext _dbContext;
		private readonly IMapper _mapper;

		public CatalogService(ApplicationDbContext dbContext, IMapper mapper)
		{
			_dbContext = dbContext;
			_mapper = mapper;
		}

		public async Task<CatalogViewModel> GetCatalog(CatalogViewModel model, int page, int pageSize)
		{
			var films = await GetFilms();

			var planets = await GetPlanets();

			var heroesQuery = _dbContext.Heroes.AsQueryable();

			if ((model.SelectedFilms != null && model.SelectedFilms.Any()))
			{
				heroesQuery = heroesQuery.Where(h => h.Films.Any(f => model.SelectedFilms.Contains(f.Id)));
			}

			if ((model.SelectedPlanets != null && model.SelectedPlanets.Any()))
			{
				heroesQuery = heroesQuery.Where(x => model.SelectedPlanets.Any(p => p == x.Planet.Id));
			}

			if (model.Gender != null)
			{
				heroesQuery = heroesQuery.Where(x => x.Gender == model.Gender);
			}

			if (model.dateFrom != null)
			{
				heroesQuery = heroesQuery.Where(x => x.BornDate > model.dateFrom);
			}

			if (model.dateTo != null)
			{
				heroesQuery = heroesQuery.Where(x => x.BornDate < model.dateTo);
			}

			return new CatalogViewModel()
			{
				Films = films,
				Heroes = await heroesQuery.Select(x => _mapper.Map<HeroViewModel>(x)).ToPagedListAsync(page, pageSize),
				Planets = planets,
				SelectedFilms = new List<int>(),
				SelectedPlanets = new List<int>(),
			};
		}

		public async Task<List<PlanetViewModel>> GetPlanets()
		{
			return await _dbContext.Planets.Select(x => _mapper.Map<PlanetViewModel>(x)).ToListAsync();
		}

		public async Task<List<FilmViewModel>> GetFilms()
		{
			return await _dbContext.Films.Select(x => _mapper.Map<FilmViewModel>(x)).ToListAsync();
		}

		public async Task<List<HeroViewModel>> GetHeroes()
		{
			return await _dbContext.Heroes.Select(x => _mapper.Map<HeroViewModel>(x)).ToListAsync();
		}
	}
}