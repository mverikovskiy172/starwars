﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using StarWars.Data.Models;
using StarWars.ViewModels;

namespace StarWars.Services.Configurations
{
	public class MapperConfiguration : Profile
	{
		public MapperConfiguration()
		{
			CreateMap<Film, FilmViewModel>().ReverseMap();
			CreateMap<Hero, HeroViewModel>().ReverseMap();
			CreateMap<Planet, PlanetViewModel>().ReverseMap();
			CreateMap<Hero, UpdateHeroViewModel>().ReverseMap();
			CreateMap<CreateHeroViewModel, Hero>().ReverseMap();
			CreateMap<UpdateHeroViewModel, HeroViewModel>().ReverseMap();
			CreateMap<IdentityUser, UserViewModel>();
		}
	}
}