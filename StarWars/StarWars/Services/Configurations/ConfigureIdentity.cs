﻿using Microsoft.AspNetCore.Identity;
using StarWars.Constants;

namespace StarWars.Services.Configurations
{
	public static class ConfigureIdentity
	{
		public static async Task ConfigureIdentityAsync(this WebApplication app)
		{
			using var serviceScope = app.Services.CreateScope();
			var userManager = serviceScope.ServiceProvider.GetService<UserManager<IdentityUser>>();
			var roleManager = serviceScope.ServiceProvider.GetService<RoleManager<IdentityRole>>();
			var config = app.Configuration.GetSection("SuperAdminConfig");

			var adminRole = await roleManager.FindByNameAsync(Roles.User);
			if (adminRole == null)
			{
				var result = await roleManager.CreateAsync(new IdentityRole()
				{
					Name = Roles.User
				});

				if (!result.Succeeded)
				{
					throw new InvalidOperationException($"Unable to create {Roles.User} role.");
				}

				adminRole = await roleManager.FindByNameAsync(Roles.User);
			}

			var superAdmin = await userManager.FindByNameAsync(config["AdminUserName"]);
			if (superAdmin == null)
			{
				var userResult = await userManager.CreateAsync(new IdentityUser()
				{
					UserName = config["AdminUserName"],
					Email = config["AdminUserName"]
				}, config["AdminPassword"]);

				if (!userResult.Succeeded)
				{
					throw new InvalidOperationException($"Unable to create {config["AdminUserName"]} user");
				}

				superAdmin = await userManager.FindByNameAsync(config["AdminUserName"]);
			}

			if (!await userManager.IsInRoleAsync(superAdmin, adminRole.Name))
			{
				await userManager.AddToRoleAsync(superAdmin, adminRole.Name);
			}
		}
	}
}