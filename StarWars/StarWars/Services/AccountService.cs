﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Identity;
using StarWars.ViewModels;
using System.Data;
using System.Security.Claims;

namespace SmtpSender.Services
{
	public interface IAccountService
	{
		Task Login(IdentityUser user);
		Task Register(RegisterViewModel model);
	}

	public class AccountService : IAccountService
	{
		private readonly RoleManager<IdentityRole> _roleManager;
		private readonly SignInManager<IdentityUser> _signInManager;
		private readonly UserManager<IdentityUser> _userManager;

		public AccountService(RoleManager<IdentityRole> roleManager, SignInManager<IdentityUser> signInManager,
			UserManager<IdentityUser> userManager)
		{
			_roleManager = roleManager;
			_signInManager = signInManager;
			_userManager = userManager;
		}

		public async Task Login(IdentityUser user)
		{
			var role = _roleManager.Roles.First();

			var claims = new List<Claim>
			{
				new("UserName", user.UserName),
				new("Id", user.Id.ToString()),
				new Claim(ClaimTypes.Role, role.Name)
			};

			var authProperties = new AuthenticationProperties
			{
				ExpiresUtc = DateTimeOffset.UtcNow.AddDays(1),
				IsPersistent = true
			};

			await _signInManager.SignInWithClaimsAsync(user, authProperties, claims);
		}

		public async Task Register(RegisterViewModel model)
		{
			var user = new IdentityUser()
			{
				Email = model.Email,
				UserName = model.Email,
			};
			var result = await _userManager.CreateAsync(user, model.Password);
			if (result.Succeeded)
			{
				await _signInManager.SignInAsync(user, false);
				return;
			}

			var errors = string.Join(", ", result.Errors.Select(x => x.Description));
			throw new ArgumentException(errors);
		}
	}
}