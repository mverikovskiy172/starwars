﻿using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;

namespace StarWars.ViewModels
{
	public class RegisterViewModel
	{
		[Required(ErrorMessage = "Email обязателен для заполнения")]
		[Display(Name = "Email")]
		public string Email { get; set; }

		[Required(ErrorMessage = "ФИО обязательно для заполнения")]
		[Display(Name = "ФИО")]
		public string Name { get; set; }

		[Required(ErrorMessage = "Пароль обязателен для заполнения")]
		[DataType(DataType.Password)]
		[Display(Name = "Пароль")]
		public string Password { get; set; }
	}
}