﻿using StarWars.Data.Enums;

namespace StarWars.ViewModels
{
	public class HeroViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameOriginal { get; set; }
        public int BornDate { get; set; }
        public PlanetViewModel Planet { get; set; }
        public string Race { get; set; }
        public string Height { get; set; }
        public string HairColor { get; set; }
        public string EyeColor { get; set; }
        public List<FilmViewModel> Films { get; set; }
        public string Description { get; set; }
        public Gender Gender { get; set; }
        public UserViewModel CreateByUser { get; set; } 
    }
}