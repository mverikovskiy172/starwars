﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace StarWars.ViewModels
{
    public class LoginViewModel
    {
        [Required]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email")]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
