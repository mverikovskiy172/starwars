﻿using StarWars.Data.Enums;
using X.PagedList;

namespace StarWars.ViewModels
{
    public class CatalogViewModel
    {
        public string? BornDate { get; set; }
        public List<PlanetViewModel>? Planets { get; set; }
        public List<FilmViewModel>? Films { get; set; }
        public IPagedList<HeroViewModel> Heroes { get; set; }
        public Gender? Gender { get; set; }

		public List<int>? SelectedFilms { get; set; }
		public List<int>? SelectedPlanets { get; set; }
		public int? dateFrom { get; set; }
		public int? dateTo { get; set; }
    }
}