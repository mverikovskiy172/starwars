﻿using StarWars.Data.Enums;

namespace StarWars.ViewModels
{
	public class UpdateHeroViewModel
	{
		public string Name { get; set; }
		public string NameOriginal { get; set; }
		public int BornDate { get; set; }
		public List<PlanetViewModel>? Planets { get; set; }
		public string Race { get; set; }
		public string Height { get; set; }
		public string HairColor { get; set; }
		public string EyeColor { get; set; }
		public List<FilmViewModel>? Films { get; set; }
		public string Description { get; set; }
		public Gender Gender { get; set; }
		public List<int> SelectedFilms { get; set; }
		public int SelectedPlanet { get; set; }
		public PlanetViewModel? Planet { get; set; }
	}
}