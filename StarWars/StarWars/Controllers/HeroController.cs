﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StarWars.Services;
using StarWars.ViewModels;
using System.Security.Authentication;
using System.Security.Claims;

namespace StarWars.Controllers
{
	public class HeroController : Controller
	{
		private readonly IHeroService _heroService;

		public HeroController(IHeroService heroService)
		{
			_heroService = heroService;
		}

		[HttpGet]
		[Route("hero/{heroId}")]
		public async Task<IActionResult> Hero(int heroId)
		{
			try
			{
				var model = await _heroService.GetHero(heroId);
				return View(model);
			}
			catch (KeyNotFoundException ex)
			{
				TempData["ErrorMessage"] = $"Персонаж с таким Id-{heroId} не существует";
			}
			catch (Exception ex)
			{
				TempData["ErrorMessage"] = "Непредвиденная ошибка";
			}

			return RedirectToAction("Catalog", "Catalog");
		}

		[HttpGet]
		[Authorize(AuthenticationSchemes = "Identity.Application")]
		[Route("hero/createhero")]
		public async Task<IActionResult> CreateHero()
		{
			try
			{
				var model = await _heroService.GetPageForCreateHero();
				return View(model);
			}
			catch (Exception ex)
			{
				TempData["ErrorMessage"] = "Непредвиденная ошибка";
			}

			return RedirectToAction("Catalog", "Catalog");
		}

		[HttpPost]
		[Authorize(AuthenticationSchemes = "Identity.Application")]
		public async Task<IActionResult> CreateHero(CreateHeroViewModel model)
		{
			if (!model.SelectedFilms.Any())
			{
				ModelState.AddModelError("", "Select films");
			}

			if (ModelState.IsValid)
			{
				try
				{
					var heroId = await _heroService.CreateHero(model, GetIdFromClaim());
					return RedirectToAction("hero", new { heroId });
				}
				catch (Exception ex)
				{
					TempData["ErrorMessage"] = "Непредвиденная ошибка";
				}

				return RedirectToAction("catalog", "catalog");
			}

			model.Planets = await _heroService.GetPlanets();
			model.Films = await _heroService.GetFilms();

			return View(model);
		}

		[HttpGet]
		[Authorize(AuthenticationSchemes = "Identity.Application")]
		[Route("hero/{heroId}/updatehero")]
		public async Task<IActionResult> UpdateHero(int heroId)
		{
			try
			{
				var model = await _heroService.GetPageForUpdateHero(heroId, GetIdFromClaim());
				return View(model);
			}
			catch (KeyNotFoundException ex)
			{
				TempData["ErrorMessage"] = $"Персонаж с таким Id-{heroId} не существует";
			}
			catch (AuthenticationException ex)
			{
				TempData["ErrorMessage"] = $"У вас нет доступа к изменению персонажа";
			}
			catch (Exception ex)
			{
				TempData["ErrorMessage"] = "Непредвиденная ошибка";
			}

			return RedirectToAction("Catalog", "Catalog");
		}

		[HttpPost]
		[Authorize(AuthenticationSchemes = "Identity.Application")]
		[Route("hero/{heroId}/updatehero")]
		public async Task<IActionResult> UpdateHero(int heroId, UpdateHeroViewModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					await _heroService.UpdateHero(heroId, model, GetIdFromClaim());
					return RedirectToAction("Hero", new { heroId });
				}
				catch (KeyNotFoundException ex)
				{
					TempData["ErrorMessage"] = $"Персонаж с таким Id-{heroId} не существует";
				}
				catch (AuthenticationException ex)
				{
					TempData["ErrorMessage"] = $"У вас нет доступа к изменению персонажа";
				}
				catch (Exception ex)
				{
					TempData["ErrorMessage"] = "Непредвиденная ошибка";
				}

				return RedirectToAction("Catalog", "Catalog");
			}

			try
			{
				model.Films = await _heroService.GetFilmsHero(heroId);
				model.Planets = await _heroService.GetPlanets();
				model.Planet = await _heroService.GetPlanetHero(heroId);
			}
			catch (Exception ex)
			{
				TempData["ErrorMessage"] = "Непредвиденная ошибка";
			}

			return View(model);
		}

		[HttpPost]
		[Authorize(AuthenticationSchemes = "Identity.Application")]
		[Route("hero/{heroId}/deletehero")]
		public async Task<IActionResult> DeleteHero(int heroId)
		{
			try
			{
				await _heroService.DeleteHero(heroId, GetIdFromClaim());
			}
			catch (KeyNotFoundException ex)
			{
				TempData["ErrorMessage"] = $"Персонаж с таким Id-{heroId} не существует";
			}
			catch (AuthenticationException ex)
			{
				TempData["ErrorMessage"] = $"У вас нет доступа к удалению персонажа";
			}
			catch (Exception ex)
			{
				TempData["ErrorMessage"] = "Непредвиденная ошибка";
			}

			return RedirectToAction("Catalog", "Catalog");
		}

		private string GetIdFromClaim()
		{
			return User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;
		}
	}
}