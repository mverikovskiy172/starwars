﻿using Microsoft.AspNetCore.Mvc;
using StarWars.Data.Enums;
using StarWars.Services;
using StarWars.ViewModels;

namespace StarWars.Controllers
{
	public class CatalogController : Controller
	{
		private readonly ICatalogService _catalogService;

		public CatalogController(ICatalogService catalogService)
		{
			_catalogService = catalogService;
		}

		[HttpGet]
		public async Task<IActionResult> Catalog(CatalogViewModel model, int? currentDateFrom, Gender? currentGender,
			List<int>? currentSelectedFilms, List<int>? currentSelectedPlanets,
			int? currentDateTo, int page = 1, int pageSize = 4)
		{
			if (model.dateFrom != null || model.dateTo != null || model.Gender != null ||
			    (model.SelectedFilms != null && model.SelectedFilms.Any()) ||
			    (model.SelectedPlanets != null && model.SelectedPlanets.Any()))
			{
				page = 1;
			}
			else
			{
				model.dateFrom = currentDateFrom;
				model.dateTo = currentDateTo;
				model.Gender = currentGender;
				model.SelectedFilms = currentSelectedFilms;
				model.SelectedPlanets = currentSelectedPlanets;
			}

			ViewBag.CurrentFilterDateFrom = model.dateFrom;
			ViewBag.CurrentFilterDateTo = model.dateTo;
			ViewBag.CurrentFilterGender = model.Gender;
			ViewBag.CurrentFilterSelectedFilms = model.SelectedFilms;
			ViewBag.CurrentFilterSelectedPlanets = model.SelectedPlanets;


			var catalogModel = await _catalogService.GetCatalog(model, page, pageSize);
			catalogModel.dateTo = model.dateTo;
			catalogModel.Gender = model.Gender;
			catalogModel.dateFrom = model.dateFrom;
			catalogModel.SelectedFilms = model.SelectedFilms;
			catalogModel.SelectedPlanets = model.SelectedPlanets;

			return View(catalogModel);
		}
	}
}