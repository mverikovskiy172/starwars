﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SmtpSender.Services;
using StarWars.ViewModels;

namespace StarWars.Controllers
{
	public class AccountController : Controller
	{
		private readonly IAccountService _accountService;
		private readonly UserManager<IdentityUser> _userManager;
		private readonly SignInManager<IdentityUser> _signInManager;

		public AccountController(IAccountService accountService, UserManager<IdentityUser> userManager,
			SignInManager<IdentityUser> signInManager)
		{
			_accountService = accountService;
			_userManager = userManager;
			_signInManager = signInManager;
		}


		[HttpGet]
		public IActionResult Login()
		{
			return View();
		}

		[HttpGet]
		public IActionResult Register()
		{
			return View();
		}

		[HttpPost]
		[AllowAnonymous]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Login(LoginViewModel model)
		{
			if (ModelState.IsValid)
			{
				var user = await _userManager.FindByNameAsync(model.Email);
				if (user != null)
				{
					var result = await _userManager.CheckPasswordAsync(user, model.Password);
					if (result)
					{
						await _accountService.Login(user);
						return RedirectToAction("Catalog", "Catalog");
					}
				}

				ModelState.AddModelError("", "Invalid email or password.");
			}

			return View(model);
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public async Task<IActionResult> Logout()
		{
			await _signInManager.SignOutAsync();
			return RedirectToAction("Login", "Account");
		}

		[HttpPost]
		public async Task<IActionResult> Register(RegisterViewModel model)
		{
			if (ModelState.IsValid)
			{
				try
				{
					await _accountService.Register(model);
					return RedirectToAction("Catalog", "Catalog");
				}
				catch (ArgumentException ex)
				{
					ModelState.AddModelError("RegistrationErrors", ex.Message);
				}
			}

			return View(model);
		}
	}
}