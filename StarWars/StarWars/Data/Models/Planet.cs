﻿namespace StarWars.Data.Models
{
    public class Planet : BaseEntity
    {
        public string Name { get; set; }
    }
}