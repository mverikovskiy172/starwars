﻿using Microsoft.AspNetCore.Identity;
using StarWars.Data.Enums;

namespace StarWars.Data.Models
{
    public class Hero : BaseEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string NameOriginal { get; set; }
        public int BornDate { get; set; }
        public Planet Planet { get; set; }
        public string Race { get; set; }
        public string Height { get; set; }
        public string HairColor { get; set; }
        public string EyeColor { get; set; }
        public List<Film> Films { get; set; }
        public string Description { get; set; }
        public Gender Gender { get; set; }
        public IdentityUser CreateByUser { get; set; }
    }
}