﻿namespace StarWars.Data.Models
{
    public class Film: BaseEntity
    {
        public string Name { get; set; }
        public List<Hero> Heroes { get; set; }
    }
}
