﻿namespace StarWars.Data.Models
{
	public abstract class BaseEntity
	{
		public int Id { get; set; }
		public DateTime CreateDateTime { get; }

		protected BaseEntity()
		{
			CreateDateTime = DateTime.Now;
		}
	}
}