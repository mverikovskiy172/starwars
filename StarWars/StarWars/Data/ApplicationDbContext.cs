﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using StarWars.Data.Models;

namespace StarWars.Data
{
	public class ApplicationDbContext : IdentityDbContext
	{
		public DbSet<Film> Films { get; set; }
		public DbSet<Hero> Heroes { get; set; }
		public DbSet<Planet> Planets { get; set; }

		public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
			: base(options)
		{
			Database.EnsureCreated();
		}

		protected override void OnModelCreating(ModelBuilder builder)
		{
			base.OnModelCreating(builder);

			builder.Entity<Hero>(o =>
			{
				o.HasMany(x => x.Films).WithMany(x => x.Heroes);
				o.HasIndex(x => x.BornDate);
			});

			builder.Entity<Film>(o => { o.HasIndex(x => x.Name); });

			builder.Entity<Planet>(o => { o.HasIndex(x => x.Name); });
		}
	}
}