﻿namespace StarWars.Data.Enums
{
	public enum Gender
	{
		Male,
		Female
	}
}